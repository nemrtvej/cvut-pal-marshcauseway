package pal;

import pal.InputParser.InputContainer;
import pal.Solver.Point;

/**
 * Created by Marek on 9. 10. 2014.
 */
public class TestSuite {

    class TestCase {
        InputContainer input;

        int solutionPoints;

        int solutionLength;

        TestCase(InputContainer input, int solutionPoints, int solutionLength) {
            this.input = input;
            this.solutionPoints = solutionPoints;
            this.solutionLength = solutionLength;
        }

        public InputContainer getInput() {
            return input;
        }

        public int getSolutionPoints() {
            return solutionPoints;
        }

        public int getSolutionLength() {
            return solutionLength;
        }
    }

    private TestCase getFirst() {
        // result is 13 4302

        Point[] points = {
                new Point(0, 100, 800),
                new Point(1, 200, 400),
                new Point(2, 300, 1200),
                new Point(3, 400, 900),
                new Point(4, 500, 100),
                new Point(5, 500, 500),
                new Point(6, 600, 700),
                new Point(7, 700, 1100),
                new Point(8, 800, 500),
                new Point(9, 900, 300),
                new Point(10, 900, 1000),
                new Point(11, 1100, 700),
                new Point(12, 1100, 1100)
        };

        return new TestCase(new InputContainer(4400, points), 13, 4302);
    }

    private TestCase getSecond() {
        // result is 13 3400

        Point[] points = {
                new Point(0, 200, 800),
                new Point(1, 300, 400),
                new Point(2, 400, 1200),
                new Point(3, 500, 900),
                new Point(4, 600, 100),
                new Point(5, 600, 500),
                new Point(6, 700, 700),
                new Point(7, 800, 1100),
                new Point(8, 900, 500),
                new Point(9, 1000, 300),
                new Point(10, 1000, 1000),
                new Point(11, 1200, 700),
                new Point(12, 1200, 1100),
        };

        return new TestCase(new InputContainer(3400, points), 10, 3054);
    }

    private TestCase getThird() {
        // result is 12 6000

        Point[] points = {
                new Point(0, 100,  100),
                new Point(1, 1100,  100),
                new Point(2, 1100,  1100),
                new Point(3, 100,  1100),
                new Point(4, 600,  200),
                new Point(5, 700,  500),
                new Point(6, 1000,  600),
                new Point(7, 700,  700),
                new Point(8, 600,  1000),
                new Point(9, 500,  700),
                new Point(10, 200,  600),
                new Point(11, 500,  500),
        };

        return new TestCase(new InputContainer(6000, points), 12, 5052);
    }



    private TestCase getFourth() {
        // result is 8 3000

        Point[] points = {
                new Point(0, 100, 100),
                new Point(1, 1100, 100),
                new Point(2, 1100, 1100),
                new Point(3, 100, 1100),
                new Point(4, 600, 200),
                new Point(5, 700, 500),
                new Point(6, 1000, 600),
                new Point(7, 700, 700),
                new Point(8, 600, 1000),
                new Point(9, 500, 700),
                new Point(10, 200, 600),
                new Point(11, 500, 500),
        };

        return new TestCase(new InputContainer(3000, points), 8, 2530);
    }

    private TestCase getFifth() {
        Point[] points = {
                new Point(0, 100, 530),
                new Point(1, 700, 100),
                new Point(2, 1300, 520),
                new Point(3, 1000, 1310),
                new Point(4, 400, 1330),
                new Point(5, 300, 620),
                new Point(6, 700, 300),
                new Point(7, 1100, 610),
                new Point(8, 900, 1100),
                new Point(9, 500, 1130),
                new Point(10, 500, 720),
                new Point(11, 700, 510),
                new Point(12, 900, 730),
                new Point(13, 800, 920),
                new Point(14, 600, 910),
        };

        return new TestCase(new InputContainer(3900, points), 13, 3819);
    }

    TestCase[] getTestCases() {
        TestCase[] testSuites = {
                this.getFirst(),
                this.getSecond(),
                this.getThird(),
                this.getFourth(),
                this.getFifth()
        };

        //return testSuites;

        TestCase[] output = {testSuites[4]};

       return output;
    }
}
