package pal;

// Marsh Causeway problem
// https://cw.felk.cvut.cz/courses/a4m33pal/task.php?task=marshcauseway
// author Marek Makovec

import pal.InputParser.InputContainer;
import pal.InputParser.Parser;
import pal.Solver.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
    public static void main(String[] args) throws IOException
    {
        InputStreamReader inputStream = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(inputStream);

        Parser parser = new Parser();

        InputContainer inputContainer = parser.parseInput(bufferedReader);
        SolutionStorage solutionStorage = new SolutionStorage();
        IntersectionResolver intersectionResolver = new IntersectionResolver();

        long startTime = System.currentTimeMillis();
        PathFactory pathFactory = new PathFactory(intersectionResolver, inputContainer.getPoints());

        Solver solver = new Solver(intersectionResolver, pathFactory, solutionStorage);
        solutionStorage.setSolver(solver);

        Solution solution = solver.solve(inputContainer);
        System.out.println((int) Math.ceil(solution.getLength()));
//        long runTime = System.currentTimeMillis() - startTime;
//        System.out.println("Runtime: " + runTime / 1000.0);
    }
}
