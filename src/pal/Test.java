package pal;

import pal.Solver.*;

public class Test {



    public static void main(String[] args) {
        TestSuite testSuite = new TestSuite();
        TestSuite.TestCase[] testCases = testSuite.getTestCases();

        int counter = 1;

        for (TestSuite.TestCase testCase: testCases) {
            System.out.println("Test case #"+counter);
            double startTime = System.currentTimeMillis();
            IntersectionResolver intersectionResolver = new IntersectionResolver();
            PathFactory pathFactory = new PathFactory(intersectionResolver, testCase.getInput().getPoints());
            SolutionStorage solutionStorage = new SolutionStorage();
            Solver solver = new Solver(intersectionResolver, pathFactory, solutionStorage);
            solutionStorage.setSolver(solver);

            System.out.println("Initialization time: " + (System.currentTimeMillis() - startTime)/1000);
            Solution solution = solver.solve(testCase.getInput());
            System.out.println("Computation time: " + (System.currentTimeMillis() - startTime)/1000);

            System.out.println("Expected number of used points: " + testCase.getSolutionPoints());
            System.out.println("Computed number of used points: " + solution.getNumberOfPoints());

            System.out.println("Expected length: " + testCase.getSolutionLength());
            System.out.println("Computed length: " + Math.ceil(solution.getLength()));

            counter++;
            System.out.println();

            System.out.println(solution.dumpGraph(testCase.getInput().getPoints()));

            System.out.println("--------------------------------------------------------------------");
            System.out.println("--------------------------------------------------------------------");
            System.out.println("--------------------------------------------------------------------");
        }
    }

    private static double computeLength(Point first, Point second) {
        return
                Math.sqrt(
                        Math.pow(second.getX() - first.getX(), 2) +
                                Math.pow(second.getY() - first.getY(), 2)
        );
    }
}
