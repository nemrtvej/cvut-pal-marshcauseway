package pal;

import pal.InputParser.InputContainer;
import pal.Solver.*;

/**
 * Created by Marek on 10. 10. 2014.
 */
public class Xxx {
    public static void main(String[] args) {
        Point[] points = {
            new Point(0, 100, 1100),
            new Point(1, 600, 1000),
            new Point(2, 1100, 1100),
            new Point(3, 1000, 600),
            new Point(4, 700, 700),
            new Point(5, 500, 700),
            new Point(6, 500, 500),
            new Point(7, 700, 500),
            new Point(8, 1100, 100),
            new Point(9, 600, 200),
            new Point(10, 100, 100),
            new Point(11, 200, 600),
        };


        InputContainer inputContainer = new InputContainer(6000, points);
        IntersectionResolver intersectionResolver = new IntersectionResolver();
        PathFactory pathFactory = new PathFactory(intersectionResolver, inputContainer.getPoints());


        Solution s = new Solution(points.length, intersectionResolver, pathFactory, points[0]);
        for(int i = 1; i<7; i++) {
            System.out.println(i + " :" + s.addPoint(points[i]));
            //System.out.println(s.dumpGraph(points));
        }
    }
}
