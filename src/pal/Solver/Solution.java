package pal.Solver;

public class Solution {

    private IntersectionResolver intersectionResolver;

    private PathFactory pathFactory;

    private Path[] paths;

    private int pathCounter = 0;

    private int pointCounter = 0;

    private Point startPoint;

    private Point lastInsertedPoint;

    private double length;

    private boolean isCompleted = false;

    public Solution(int numberOfPoints, IntersectionResolver intersectionResolver, PathFactory pathFactory, Point point) {
        numberOfPoints = numberOfPoints+1; // first and last point will be stored twice
        this.intersectionResolver = intersectionResolver;
        this.pathFactory = pathFactory;
        this.paths = new Path[numberOfPoints];
        this.pointCounter = 1;
        this.startPoint = point;
        this.lastInsertedPoint = point;
    }

    public Solution(Solution oldSolution) {
        this.intersectionResolver = oldSolution.intersectionResolver;
        this.pathFactory = oldSolution.pathFactory;

        this.paths = new Path[oldSolution.paths.length];

        System.arraycopy(oldSolution.paths, 0, this.paths, 0, oldSolution.paths.length);

        this.startPoint = oldSolution.startPoint;
        this.lastInsertedPoint = oldSolution.lastInsertedPoint;
        this.length = oldSolution.length;
        this.pointCounter = oldSolution.pointCounter;
        this.pathCounter = oldSolution.pathCounter;
    }

    public void removeLast()
    {
        this.lastInsertedPoint.used = false;
        this.pointCounter--;

        this.lastInsertedPoint = this.paths[pathCounter-1].getStartPoint();
        this.length -= this.paths[pathCounter-1].length;
        this.pathCounter--;
    }

    public boolean addPoint(Point point) {
        this.pointCounter++;
        this.addPath(this.pathFactory.pathStorage[this.lastInsertedPoint.index][point.index]);
        this.lastInsertedPoint = point;
        point.used = true;

        return true;
    }

    public double getLength()
    {
        return this.length + this.pathFactory.pathStorage[this.lastInsertedPoint.index][this.startPoint.index].length;
    }

    public int getNumberOfPoints()
    {
        return this.pointCounter;
    }

    public String getLineString() {
        return "";
    }

    public Solution duplicate() {
        return new Solution(this);
    }

    public String dumpGraph(Point[] allPoints)
    {
        return
            "Show[" +"  Graphics[" +
                "    {" +
                "      PointSize[Large]," +
                "      Point["+ this.getPointString(allPoints) +"]," +
                "      PointSize[Small]," +
                "      Line["+ this.getLineString() +"]" +
                "    }" +
                "  ]," +
                "  Axes -> True" +
                "]";
    }

    public boolean complete() {

        if (this.pointCounter > 2 && this.isPointAppendable(this.startPoint)) {
            this.isCompleted = true;
            return true;
        }

        return false;
    }

    private void addPath(Path p) {
        this.paths[pathCounter++] = p;
        this.length += p.length;
    }

    public boolean isPointAppendable(Point point) {
        Path newPath = this.pathFactory.pathStorage[this.lastInsertedPoint.index][point.index];

        boolean result = true;

        for (int i = 0;result && i < this.pathCounter-1; i++) { // last Path does not need to be checked.
            result = !this.intersectionResolver.intersectionMatrix[this.paths[i].index][newPath.index];
        }

        return result;
    }

    public boolean isPointAppendable(Point point, double maxLength) {
        Path newPath = this.pathFactory.pathStorage[this.lastInsertedPoint.index][point.index];

        boolean result = this.length + newPath.length + this.pathFactory.pathStorage[point.index][startPoint.index].length < maxLength;

        for (int i = 0;result && i < this.pathCounter-1; i++) { // last Path does not need to be checked.
            //result = !this.intersectionResolver.intersectsLine(this.paths[i], newPath);
            result = !this.intersectionResolver.intersectionMatrix[this.paths[i].index][newPath.index];
        }

        return result;
    }

    private String getPointString(Point[] allPoints) {
        String output = "{";

        for (int i = 0; i < allPoints.length; i++) {
            output += "{" + allPoints[i].getX() + ", " + allPoints[i].getY() + "}";

            if (i != allPoints.length -1) {
                output += ", ";
            }
        }

        output += "}";

        return output;
    }
}
