package pal.Solver;

import java.awt.geom.Line2D;

public class Path {

    private Point startPoint;

    private Point endPoint;

    private Line2D.Double line;

    public double length;

    public int index;

    public Path(int index, Point startPoint, Point endPoint) {
        this.index = index;
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.line = new Line2D.Double(startPoint.getX(), startPoint.getY(), endPoint.getX(), endPoint.getY());

        this.length = Math.sqrt(
                        Math.pow(endPoint.getX() - startPoint.getX(), 2) +
                                Math.pow(endPoint.getY() - startPoint.getY(), 2)
        );
    }

    public Path(int index, Point startPoint, Point endPoint, double length) {
        this.line = new Line2D.Double(startPoint.getX(), startPoint.getY(), endPoint.getX(), endPoint.getY());
        this.index = index;
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.length = length;
    }

    public int getIndex() {
        return index;
    }

    public double getLength() {
        return this.length;
    }

    public double getX1() {
        return this.startPoint.getX();
    }

    public double getY1() {
        return this.startPoint.getY();
    }

    public double getX2() {
        return this.endPoint.getX();
    }

    public double getY2() {
        return this.endPoint.getY();
    }

    public Point getStartPoint() {
        return this.startPoint;
    }

    public Point getEndPoint() {
        return this.endPoint;
    }

    public boolean contains(Point p) {
        return this.line.contains(p);
    }

    @Override
    public String toString() {
        return "Path{" +
                "index=" + index +
                ", startPoint=" + startPoint +
                ", endPoint=" + endPoint +
                '}';
    }
}
