package pal.Solver;

public class SolutionStorage {

    private Solution bestSolution;

    private Solver solver;

    public void addSolution(Solution solution) {
        if ((this.bestSolution == null || this.isBetterSolution(solution)) && solution.complete()) {
            this.bestSolution = solution.duplicate();

            if (solution.getNumberOfPoints() == this.solver.getNumberOfPoints()) {
                this.solver.setMaxLength(solution.getLength());
            }
        }
    }

    public boolean hasSolution() {
        return this.bestSolution != null;
    }

    public Solution getSolution() {
        assert this.hasSolution(): "There must always be a solution";

        return this.bestSolution;
    }

    public void setSolver(Solver solver) {
        this.solver = solver;
    }

    public boolean isBetterSolution(Solution solution) {
        return (
            (solution.getNumberOfPoints() > this.bestSolution.getNumberOfPoints()) ||
            (solution.getNumberOfPoints() == this.bestSolution.getNumberOfPoints() && solution.getLength() < this.bestSolution.getLength())
        );
    }
}
