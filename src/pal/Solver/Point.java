package pal.Solver;

@SuppressWarnings("serial")
public class Point extends java.awt.Point {

    public boolean used;

    public int index;

    public Point(int index, int x, int y) {
        super(x, y);
        this.index = index;
    }

    public int getIndex() {
        return this.index;
    }
}
