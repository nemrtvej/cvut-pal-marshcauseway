package pal.Solver;

public class PathFactory {

    IntersectionResolver intersectionResolver;

    public Path[][] pathStorage;

    public PathFactory(IntersectionResolver intersectionResolver, Point[] points) {
        this.intersectionResolver = intersectionResolver;
        this.pathStorage = new Path[points.length][points.length];

        this.initializePaths(points);
    }

    public Path getPath(Point startPoint, Point endPoint) {
        return this.pathStorage[startPoint.index][endPoint.index];
    }

    private void initializePaths(Point[] points) {
        int counter = 0;

        int numberOfPaths = (int) ((points.length + 1.0) / 2.0 * points.length);
        Path[] allComputedPathCollection = new Path[numberOfPaths];

        this.intersectionResolver.initializeMatrix(numberOfPaths);

        for (int i = 0; i < points.length; i++) {
            for (int j = i; j < points.length; j++) {
                Point startPoint = points[i];
                Point endPoint = points[j];
                Path path = new Path(counter, startPoint, endPoint);
                Path inversedPath = new Path(counter++, endPoint, startPoint, path.length);
                this.pathStorage[startPoint.index][endPoint.index] = path;
                this.pathStorage[endPoint.index][startPoint.index] = inversedPath;

                this.intersectionResolver.computePathIntersection(path, allComputedPathCollection);
                allComputedPathCollection[path.index] = path;
            }
        }
    }
}
