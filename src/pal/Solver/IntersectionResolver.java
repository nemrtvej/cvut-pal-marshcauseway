package pal.Solver;

import java.awt.geom.Point2D;

public class IntersectionResolver {
    public boolean[][] intersectionMatrix;

    public void initializeMatrix(int size) {
        this.intersectionMatrix = new boolean[size][size];
    }

    public void computePathIntersection(Path path, Path[] currentPaths) {
        for (int i = 0; i<path.index; i++) {
            this.saveResult(path, currentPaths[i], this.hasIntersection(path, currentPaths[i]));
        }
    }

    public boolean intersectsLine(Path firstPath, Path secondPath) {
        return this.intersectionMatrix[firstPath.index][secondPath.index];
    }

    private boolean hasIntersection(Path first, Path second) {
        Point result = this.intersection(first, second);

        if (!first.contains(result) || second.contains(result)) {
            return false;
        }

        return !(result == null || result.equals(first.getStartPoint()) || result.equals(first.getEndPoint()));
    }

    /**
     * Computes the intersection between two segments.
     * @param x1 Starting point of Segment 1
     * @param y1 Starting point of Segment 1
     * @param x2 Ending point of Segment 1
     * @param y2 Ending point of Segment 1
     * @param x3 Starting point of Segment 2
     * @param y3 Starting point of Segment 2
     * @param x4 Ending point of Segment 2
     * @param y4 Ending point of Segment 2
     * @return Point where the segments intersect, or null if they don't
     */
    private Point intersection(Path first, Path second) {
        int x1 = (int) first.getX1();
        int y1 = (int) first.getY1();
        int x2 = (int) first.getX2();
        int y2 = (int) first.getY2();
        int x3 = (int) second.getX1();
        int y3 = (int) second.getY1();
        int x4 = (int) second.getX2();
        int y4 = (int) second.getY2();

        int d = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
        if (d == 0) return null;

        int xi = ((x3-x4)*(x1*y2-y1*x2)-(x1-x2)*(x3*y4-y3*x4))/d;
        int yi = ((y3-y4)*(x1*y2-y1*x2)-(y1-y2)*(x3*y4-y3*x4))/d;


        Point p = new Point(-1, xi,yi);
        if (xi < Math.min(x1,x2) || xi > Math.max(x1,x2)) return null;
        if (xi < Math.min(x3,x4) || xi > Math.max(x3,x4)) return null;

        return p;
    }

    private void saveResult(Path first, Path second, boolean hasIntersection) {
        this.intersectionMatrix[first.index][second.index] = hasIntersection;
        this.intersectionMatrix[second.index][first.index] = hasIntersection;
    }
}
