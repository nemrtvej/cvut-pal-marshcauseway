package pal.Solver;

import pal.InputParser.InputContainer;

import java.io.File;

public class Solver {

    private IntersectionResolver intersectionResolver;

    private PathFactory pathFactory;

    private SolutionStorage solutionStorage;

    private double maxLength;

    private int numberOfPoints;

    private int loopStartIndex;

    public Solver(IntersectionResolver intersectionResolver, PathFactory pathFactory, SolutionStorage solutionStorage) {
        this.intersectionResolver = intersectionResolver;
        this.pathFactory = pathFactory;
        this.solutionStorage = solutionStorage;
    }

    public Solution solve(InputContainer inputContainer) {
        Point[] points = inputContainer.getPoints();

        this.maxLength = inputContainer.getMaximumLength();

        this.loopStartIndex = 0;

        this.numberOfPoints = points.length;

        for (Point point: points) {


            Solution solution = new Solution(points.length, this.intersectionResolver, this.pathFactory, point);
            //ISolution solution = new FileDumpProxySolution(new File("log.txt"), pureSolution, inputContainer.getPoints());

            point.used = true;
            this.findSolution(solution, points);

            if (this.solutionStorage.hasSolution() && this.solutionStorage.getSolution().getNumberOfPoints() == this.numberOfPoints) {
                break;
            }
            this.numberOfPoints--;
            this.loopStartIndex++;
            if (this.solutionStorage.hasSolution() && this.solutionStorage.getSolution().getNumberOfPoints() == this.numberOfPoints) {
                this.maxLength = this.solutionStorage.getSolution().getLength();
            }
        }

        return solutionStorage.getSolution();
    }

    public int getNumberOfPoints() {
        return numberOfPoints;
    }

    public double getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(double maxLength) {
        this.maxLength = maxLength;
    }

    private void findSolution(Solution solution, Point[] points) {
        Point point;
        for (int i = this.loopStartIndex; i < points.length; i++) {
            point = points[i];

            if (point.used || !solution.isPointAppendable(point, this.maxLength)) {
                continue;
            }

            solution.addPoint(point);

            solutionStorage.addSolution(solution);

            if (solution.getNumberOfPoints() == this.numberOfPoints) {
                solution.removeLast();
                return;
            }

            this.findSolution(solution, points);

            solution.removeLast();
        }
    }
}
