package pal.InputParser;

import pal.Solver.Point;

import java.io.BufferedReader;
import java.io.IOException;

public class Parser {

    public InputContainer parseInput(BufferedReader bufferedReader) throws IOException {
        int maxLength;
        int numberOfPoints;

        String inputLine;

        inputLine = bufferedReader.readLine();

        String[] parts = inputLine.split(" ");

        numberOfPoints = Integer.parseInt(parts[0]);
        maxLength = Integer.parseInt(parts[1]);

        Point[] points = new Point[numberOfPoints];

        for (int i = 0; i < numberOfPoints; i++) {
            inputLine = bufferedReader.readLine();
            parts = inputLine.split(" ");

            Point point = new Point(
                i,
                Integer.parseInt(parts[0]),
                Integer.parseInt(parts[1])
            );

            points[i] = point;
        }

        return new InputContainer(maxLength, points);
    }

}
