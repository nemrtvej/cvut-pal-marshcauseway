package pal.InputParser;

import pal.Solver.Point;

public class InputContainer {

    private int maximumLength;

    private Point[] points;

    public InputContainer(int maximumLength, Point[] points) {
        this.maximumLength = maximumLength;
        this.points = points;
    }

    public int getMaximumLength() {
        return maximumLength;
    }

    public Point[] getPoints() {
        return points;
    }

    public String getPointString() {
        String output = "{";

        for (int i = 0; i < this.points.length; i++) {
            output += "{" + this.points[i].getX() + ", " + this.points[i].getY() + "}";

            if (i != this.points.length -1) {
                output += ", ";
            }
        }

        output += "}";

        return output;
    }
}
